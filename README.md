# Список заданий
--------------------
* ### Оптимизированный бэкэнд под сайт с отелями [Optimized](https://bitbucket.org/datife10/byl/src/master/optimized/)
* ### Неоптимизированный бэкэнд под сайт с отелями + тест [Unoptimized](https://bitbucket.org/datife10/byl/src/master/unoptimized/)
* ### Результаты скорости выполнения оптимизации [Result](https://bitbucket.org/datife10/byl/src/master/Result.xlsx)
* ### Сайт записей на отель [WebPage with Flask](https://bitbucket.org/datife10/byl/src/master/webPage/)
* ### Визуализация результатов [Visualization](https://bitbucket.org/datife10/byl/src/master/Visualization/)
* ### Инд. зад. №1 [вариант 2](https://bitbucket.org/datife10/byl/src/master/Lab1/)
* ### Инд. зад. №2 [вариант 7](https://bitbucket.org/datife10/byl/src/master/Lab2/)
* ### Инд. зад. №3 [вариант 1](https://bitbucket.org/datife10/byl/src/master/Lab3/)





