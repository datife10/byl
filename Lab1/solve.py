class Calculator():

    def __init__(self):
        pass

    def add(self, number1, number2):
        return number1+number2

    def subtract(self, number1, number2):
        return number1-number2       

    def multiply(self, number1, number2):
        return number1*number2

    def divide(self, number1, number2):
        return number1/number2    


def main():
    calculator = Calculator()

    print(calculator.add(10, 5))
    print(calculator.subtract(10, 5))
    print(calculator.multiply(10, 5))
    print(calculator.divide(10, 5))
   