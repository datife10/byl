import Test
from solve import Calculator

calculator = Calculator()
Test.assert_equals(calculator.add(5, 5), 10, "5 + 5 = 10")
Test.assert_equals(calculator.add(20,5), 25, "20 + 5 = 25") 
Test.assert_equals(calculator.subtract(30,5), 25, "30 - 5 = 25")
Test.assert_equals(calculator.subtract(100,5), 95, "100 - 5 = 95")
Test.assert_equals(calculator.multiply(5,5), 25, "5 * 5 = 25")
Test.assert_equals(calculator.multiply(100,5), 500, "100 * 5 = 500")
Test.assert_equals(calculator.divide(10,5), 2, "10 / 5 = 2")
Test.assert_equals(calculator.divide(100,5), 20, "100 / 5 = 20")