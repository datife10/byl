def valuePi(k):
    n = 2
    pi = 3

    for i in range(2, 2 + k):
        if (i % 2 == 0):
            pi += 4 / (n * (n + 1) * (n + 2))
        else: 
            pi -= 4 / (n * (n + 1) * (n + 2))
        n += 2

    return pi 