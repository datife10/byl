import Test
from solve import defferentTemp

Test.assert_equals(defferentTemp([73,74,75,71,69,72,76,73]), [1,1,4,2,1,1,0,0])
Test.assert_equals(defferentTemp([73,74,75,71,69,72,76,73]), [1,1,4,0,1,1,0,0])
