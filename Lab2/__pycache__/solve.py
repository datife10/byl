def defferentTemp(arr):
    ans = []

    for i in range(0, len(arr)):
        j = i

        while (j < len(arr) and arr[j] <= arr[i]):
            j += 1
        
        if j == len(arr):
            ans.append(0)
        else: 
            ans.append(j - i)
    
    return ans